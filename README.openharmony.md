# 通过新建子系统并在该子系统的部件下添加模块的方式添加stress-ng

## 一、使用方法

**1.在third_party目录下建立stress_ng目录**

**2.在此stress_ng目录下输入命令**

```shell
git clone  路径地址  stress_ng_master
```

**3.将stress_ng_master目录下的ohos.build文件移动到stress_ng目录下**

```shell
$ cd stress_ng_master
$ mv ohos.build ../
$ ls
$ ohos.build  stress_ng_master
```



## 二、外部配置改动

1.//vendor/hisilicon/hispark_taurus_standard目录config.json文件。(基于hi3516dv300)

#包围的为新增部分

```
###########################
{
      "subsystem": "stress_ng",
      "components": [
        {
          "component": "stress_ng",
          "features": []
        }
      ]
    },
#############################    
```

(基于rk3568)修改路径文件为//vendor/hihope/rk3568/config.json

```
#############################
{
      "subsystem": "stress_ng",
      "components": [
        {
          "component": "stress_ng",
          "features": []
        }
      ]
    },
###################################    
```



2.build目录下的subsystem_config.json文件。

#包围的为新增部分

```
{
  "ace": {
    "path": "foundation/ace",
    "name": "ace"
  },
############################################
  "stress_ng": {
    "path": "third_party/stress_ng",
    "name": "stress_ng"
  },
############################################
  "ai": {
    "path": "foundation/ai",
    "name": "ai"
  },
```

3.修改third_party/musl/include/endian.h文件使编译通过

#包围的为新增部分

```
#ifndef _ENDIAN_H
#define _ENDIAN_H

####################################################################
#pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
#pragma clang diagnostic ignored "-Wshift-op-parentheses"
####################################################################

#include <features.h>
```

## 三、编译命令

```
hb set -root .
(hispark_taurus_standard) 或者 （rk3568）
hb build -T stress_ng
```

## 四、结果

可在```out/rk3568/(out/hispark_taurus)```下看到编译成功的stress-ng

尝试```file stress-ng```

结果为：

```
stress-ng: ELF 32-bit LSB shared object, ARM, EABI5 version 1 (SYSV), dynamically linked, interpreter /lib/ld-musl-arm.so.1, BuildID[md5/uuid]=495f2ef5913a4881fdf06e90d8eb8332, stripped
```

