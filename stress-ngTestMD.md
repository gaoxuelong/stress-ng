#                                 stress-ng测试文档

## 一.stress-ng的测试实例：

### 1.1 在hi3516dv300测试

### 1.2 ./stress-ng --cpu N --vm N --hdd N --fork N --timeout T --metrics的使用

--cpu N:代表进程个数（每个进程会占用一个cpu，当超出cpu个数时，进程间会互相争用cpu）

--vm N: 在mmap上的压力个数

--hdd N: N个在write()/unlink()的压力

--fork N: N个在fork()和exit()上的压力

--timeout：表示测试时长

--metrics: 打印活动的伪指标

**命令：**

```shell
./stress-ng --cpu 4 --vm 2 --hdd 1 --fork 8 --timeout 2m --metrics
```

**结果：**

```
stress-ng: info:  [7316] setting to a 120 second (2 mins, 0.00 secs) run per stressor
stress-ng: info:  [7316] dispatching hogs: 4 cpu, 2 vm, 1 hdd, 8 fork
stress-ng: info:  [7316] successful run completed in 123.07s (2 mins, 3.07 secs)
stress-ng: info:  [7316] stressor       bogo ops real time  usr time  sys time   bogo ops/s     bogo ops/s CPU used per
stress-ng: info:  [7316]                           (secs)    (secs)    (secs)   (real time) (usr+sys time) instance (%)
stress-ng: info:  [7316] cpu                 734    121.00     10.24      0.05         6.07          71.33         2.13
stress-ng: info:  [7316] vm                 7009    121.49      0.00      0.00        57.69           0.00         0.00
stress-ng: info:  [7316] hdd                2136    122.51      1.38      1.34        17.44         785.29         2.22
stress-ng: info:  [7316] fork               9577    120.11      0.56      5.55        79.74        1567.43         0.64
```

### 1.3 ./stress-ng --matrix -1 -t 60的使用

--matrix:矩形运算

**命令1：**

```shell
./stress-ng --matrix -1 -t 60
```

**结果：**

```
stress-ng: info:  [1809] setting to a 60 second run per stressor
stress-ng: info:  [1809] dispatching hogs: 2 matrix
stress-ng: info:  [1809] successful run completed in 60.01s (1 min, 0.01 secs)**
```

**命令2：**

--metrics-brief：启用指标并仅显示非零结果

```shell
./stress-ng --matrix 0 -t 60s --metrics-brief
```

**结果：**

```
stress-ng: info:  [1805] setting to a 60 second run per stressor
stress-ng: info:  [1805] dispatching hogs: 2 matrix
stress-ng: info:  [1805] successful run completed in 60.00s (1 min, 0.00 secs)
stress-ng: info:  [1805] stressor       bogo ops real time  usr time  sys time   bogo ops/s     bogo ops/s
stress-ng: info:  [1805]                           (secs)    (secs)    (secs)   (real time) (usr+sys time)
stress-ng: info:  [1805] matrix            21120     60.00    116.07      0.14       352.00         181.74
```

### 1.4 ./stress-ng --iomix 4 --smart -t 30s的使用

--iomix: 启动 N 个混合 I/O 操作的工作程序

--smart:显示 S.M.A.R.T. 的变化数据

**命令：**

```shell
./stress-ng --iomix 4 --smart -t 30s
```

**结果：**

```
stress-ng: info:  [1816] setting to a 30 second run per stressor
stress-ng: info:  [1816] dispatching hogs: 4 iomix
stress-ng: info:  [1816] successful run completed in 30.14s
stress-ng: info:  [1816] could not find any S.M.A.R.T. enabled devices
注：不能找到任何S.M.A.R.T.使能设备
```

### 1.5 ./stress-ng --branch 1 --perf -t 10 >2& 1 |grep Branch的使用

**命令：**

```shell
./stress-ng --branch 1 --perf -t 10 >2& 1 |grep Branch
```

**结果：**

```
stress-ng: info:  [1897] setting to a 10 second run per stressor
stress-ng: info:  [1897] dispatching hogs: 1 branch
stress-ng: info:  [1897] successful run completed in 12.83s
stress-ng: info:  [1897] branch:
stress-ng: info:  [1897]                 8513567843 CPU Cycles                      0.66 B/sec
stress-ng: info:  [1897]                 2233893715 Instructions                    0.17 B/sec (0.262 instr. per cycle)
stress-ng: info:  [1897]                  502447323 Branch Instructions            39.17 M/sec
stress-ng: info:  [1897]                  480683304 Branch Misses                  37.48 M/sec (95.67%)
stress-ng: info:  [1897]                 8709886039 Bus Cycles                      0.68 B/sec
stress-ng: info:  [1897]                 1029455126 Cache References               80.26 M/sec
stress-ng: info:  [1897]                    1467862 Cache Misses                    0.11 M/sec ( 0.14%)
stress-ng: info:  [1897]                 1032520267 Cache L1D Read                 80.50 M/sec
stress-ng: info:  [1897]                    1314330 Cache L1D Read Miss             0.10 M/sec
stress-ng: info:  [1897]                 1033404896 Cache L1D Write                80.57 M/sec
stress-ng: info:  [1897]                    1447739 Cache L1D Write Miss            0.11 M/sec
stress-ng: info:  [1897]                 4568109498 Cache L1I Read                  0.36 B/sec
stress-ng: info:  [1897]                    2138121 Cache L1I Read Miss             0.17 M/sec
stress-ng: info:  [1897]                    6615061 Cache LL Read                   0.52 M/sec
stress-ng: info:  [1897]                    1238741 Cache LL Read Miss             96.58 K/sec
stress-ng: info:  [1897]                   12830870 Cache LL Write                  1.00 M/sec
stress-ng: info:  [1897]                          0 Cache LL Write Miss             0.00 /sec
stress-ng: info:  [1897]                          0 Cache DTLB Read Miss            0.00 /sec
stress-ng: info:  [1897]                          0 Cache DTLB Write Miss           0.00 /sec
stress-ng: info:  [1897]                          0 Cache ITLB Read Miss            0.00 /sec
stress-ng: info:  [1897]                          0 Cache ITLB Write Miss           0.00 /sec
stress-ng: info:  [1897]                          0 Cache BPU Read                  0.00 /sec
stress-ng: info:  [1897]                          0 Cache BPU Read Miss             0.00 /sec
stress-ng: info:  [1897]                          0 Cache BPU Write                 0.00 /sec
stress-ng: info:  [1897]                          0 Cache BPU Write Miss            0.00 /sec
stress-ng: info:  [1897]                 9791573580 CPU Clock                       0.76 B/sec
stress-ng: info:  [1897]                 9794492700 Task Clock                      0.76 B/sec
stress-ng: info:  [1897]                          6 Page Faults Total               0.47 /sec
stress-ng: info:  [1897]                          6 Page Faults Minor               0.47 /sec
stress-ng: info:  [1897]                          0 Page Faults Major               0.00 /sec
stress-ng: info:  [1897]                       1568 Context Switches              122.25 /sec
stress-ng: info:  [1897]                          1 CPU Migrations                  0.08 /sec
stress-ng: info:  [1897]                          0 Alignment Faults                0.00 /sec
stress-ng: info:  [1897]                          0 Emulation Faults                0.00 /sec
stress-ng: info:  [1897]                        108 System Call Enter               8.42 /sec
stress-ng: info:  [1897]                        107 System Call Exit                8.34 /sec
stress-ng: info:  [1897]                          0 Kmalloc                         0.00 /sec
stress-ng: info:  [1897]                          0 Kmalloc Node                    0.00 /sec
stress-ng: info:  [1897]                          5 Kfree                           0.39 /sec
stress-ng: info:  [1897]                          3 Kmem Cache Alloc                0.23 /sec
stress-ng: info:  [1897]                          0 Kmem Cache Alloc Node           0.00 /sec
stress-ng: info:  [1897]                         21 Kmem Cache Free                 1.64 /sec
stress-ng: info:  [1897]                          1 MM Page Alloc                   0.08 /sec
stress-ng: info:  [1897]                          1 MM Page Free                    0.08 /sec
stress-ng: info:  [1897]                       5190 RCU Utilization               404.63 /sec
stress-ng: info:  [1897]                          6 Sched Migrate Task              0.47 /sec
stress-ng: info:  [1897]                          0 Sched Move NUMA                 0.00 /sec
stress-ng: info:  [1897]                       2121 Sched Wakeup                  165.36 /sec
stress-ng: info:  [1897]                          0 Sched Proc Exec                 0.00 /sec
stress-ng: info:  [1897]                          0 Sched Proc Exit                 0.00 /sec
stress-ng: info:  [1897]                          0 Sched Proc Fork                 0.00 /sec
stress-ng: info:  [1897]                          1 Sched Proc Free                 0.08 /sec
stress-ng: info:  [1897]                          0 Sched Proc Wait                 0.00 /sec
stress-ng: info:  [1897]                       1568 Sched Switch                  122.25 /sec
stress-ng: info:  [1897]                         57 Signal Generate                 4.44 /sec
stress-ng: info:  [1897]                          1 Signal Deliver                  0.08 /sec
stress-ng: info:  [1897]                       1939 IRQ Entry                     151.17 /sec
stress-ng: info:  [1897]                       1939 IRQ Exit                      151.17 /sec
stress-ng: info:  [1897]                        258 Soft IRQ Entry                 20.11 /sec
stress-ng: info:  [1897]                        258 Soft IRQ Exit                  20.11 /sec
stress-ng: info:  [1897]                          1 Writeback Dirty Inode           0.08 /sec
stress-ng: info:  [1897]                          0 Writeback Dirty Page            0.00 /sec
stress-ng: info:  [1897]                          0 Migrate MM Pages                0.00 /sec
stress-ng: info:  [1897]                          0 SKB Consume                     0.00 /sec
stress-ng: info:  [1897]                          0 SKB Kfree                       0.00 /sec
stress-ng: info:  [1897]                          0 Filemap page-cache add          0.00 /sec
stress-ng: info:  [1897]                          0 Filemap page-cache del          0.00 /sec
stress-ng: info:  [1897]                          0 OOM Compact Retry               0.00 /sec
stress-ng: info:  [1897]                          0 OOM Wake Reaper                 0.00 /sec
```



### 1.6 ./stress-ng --cpu 2 --matrix 1 --mq 3 -t 1m的使用

--mq: 启动 N 个使用 POSIX 消息传递消息的工作

**命令：**

```shell
./stress-ng --cpu 2 --matrix 1 --mq 3 -t 1m
```

**结果：**

```
stress-ng: info:  [1906] setting to a 60 second run per stressor
stress-ng: info:  [1906] dispatching hogs: 2 cpu, 1 matrix, 3 mq
stress-ng: info:  [1906] successful run completed in 60.45s (1 min, 0.45 secs)
```

### 1.7 ./stress-ng --seq 4 -t 20的使用

**命令：**

```shell
./stress-ng --seq 4 -t 20
```

**结果：**

```
stress-ng: info:  [3656] apparmor: stressor will be skipped, AppArmor is not available
stress-ng: info:  [3656] bad_ioctl stressor will be skipped, need to be running without root privilege for this stressorstress-ng: info:  [3656] cpu_online stressor will be skipped, cannot write to cpu1 online sysfs control file
stress-ng: info:  [3656] efivar stressor will be skipped, need to have access to EFI vars in /sys/firmware/efi/vars
stress-ng: info:  [3656] exec stressor must not run as root, skipping the stressor
stress-ng: info:  [3656] fanotify stressor will be skipped, : system call not supported
stress-ng: info:  [3656] /sys/kernel/mm/page_idle/bitmap stressor will be skipped, cannot access file idle_page
stress-ng: info:  [3656] ipsec_mb: stressor will be skipped, CPU needs to be an x86-64 and a recent IPSec MB library is required.
stress-ng: info:  [3656] rdrand stressor will be skipped, CPU does not support the rdrand instruction.
stress-ng: info:  [3656] seccomp stressor will be skipped, the check for seccomp failed, wait failed: errno=10 (No child process)
stress-ng: info:  [3656] spawn stressor must not run as root, skipping the stressor
stress-ng: info:  [3656] tsc stressor will be skipped, CPU does not support the rdtsc instruction.
stress-ng: info:  [3656] tun stressor will be skipped, cannot open /dev/net/tun
stress-ng: info:  [3656] usersyscall: prctl user dispatch is not working, skipping the stressor
stress-ng: info:  [3656] disabled 'bind-mount' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] disabled 'mlockmany' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] disabled 'oom-pipe' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] disabled 'smi' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] disabled 'sysinval' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] disabled 'watchdog' as it may hang or reboot the machine (enable it with the --pathological option)
stress-ng: info:  [3656] setting to a 20 second run per stressor
stress-ng: info:  [3656] dispatching hogs: 4 access, 4 af-alg, 4 affinity, 4 aio, 4 aiol, 4 alarm, 4 atomic, 4 bad-altstack, 4 bigheap, 4 binderfs, 4 branch, 4 brk, 4 bsearch, 4 cache, 4 cap, 4 chattr, 4 chdir, 4 chmod, 4 chown, 4 chroot, 4 clock, 4 clone, 4 close, 4 context, 4 copy-file, 4 cpu, 4 crypt, 4 cyclic, 4 daemon, 4 dccp, 4 dentry, 4 dev, 4 dev-shm, 4 dir, 4 dirdeep, 4 dirmany, 4 dnotify, 4 dup, 4 dynlib, 4 enosys, 4 env, 4 epoll, 4 eventfd, 4 exit-group, 4 fallocate, 4 fault, 4 fcntl, 4 fiemap, 4 fifo, 4 file-ioctl, 4 filename, 4 flock, 4 fork, 4 fp-error, 4 fpunch, 4 fstat, 4 full, 4 funccall, 4 funcret, 4 futex, 4 get, 4 getdent, 4 getrandom, 4 goto, 4 handle, 4 hash, 4 hdd, 4 heapsort, 4 hrtimers, 4 hsearch, 4 icache, 4 icmp-flood, 4 inode-flags, 4 inotify, 4 io, 4 iomix, 4 ioport, 4 ioprio, 4 io-uring, 4 itimer, 4 judy, 4 kcmp, 4 key, 4 kill, 4 klog, 4 kvm, 4 l1cache, 4 landlock, 4 lease, 4 link, 4 list, 4 loadavg, 4 locka, 4 lockbus, 4 lockf, 4 lockofd, 4 longjmp, 4 loop, 4 lsearch, 4 madvise, 4 malloc, 4 matrix, 4 matrix-3d, 4 mcontend, 4 membarrier, 4 memcpy, 4 memfd, 4 memhotplug, 4 memrate, 4 memthrash, 4 mergesort, 4 mincore, 4 misaligned, 4 mknod, 4 mlock, 4 mmap, 4 mmapaddr, 4 mmapfixed, 4 mmapfork, 4 mmaphuge, 4 mmapmany, 4 mq, 4 mremap, 4 msg, 4 msync, 4 munmap, 4 nanosleep, 4 netdev, 4 netlink-proc, 4 netlink-task, 4 nice, 4 nop, 4 null, 4 numa, 4 opcode, 4 open, 4 pageswap, 4 pci, 4 personality, 4 physpage, 4 pidfd, 4 ping-sock, 4 pipe, 4 pipeherd, 4 pkey, 4 poll, 4 prctl, 4 prefetch, 4 procfs, 4 pthread, 4 ptrace, 4 pty, 4 qsort, 4 quota, 4 radixsort, 4 randlist, 4 ramfs, 4 rawdev, 4 rawpkt, 4 rawsock, 4 rawudp, 4 readahead, 4 reboot, 4 remap, 4 rename, 4 resched, 4 resources, 4 revio, 4 rlimit, 4 rmap, 4 rseq, 4 rtc, 4 schedpolicy, 4 sctp, 4 seal, 4 secretmem, 4 seek, 4 sem, 4 sem-sysv, 4 sendfile, 4 session, 4 set, 4 shellsort, 4 shm, 4 shm-sysv, 4 sigabrt, 4 sigchld, 4 sigfd, 4 sigfpe, 4 sigio, 4 signal, 4 signest, 4 sigpending, 4 sigpipe, 4 sigq, 4 sigrt, 4 sigsegv, 4 sigsuspend, 4 sigtrap, 4 skiplist, 4 sleep, 4 sock, 4 sockabuse, 4 sockdiag, 4 sockfd, 4 sockpair, 4 sockmany, 4 softlockup, 4 sparsematrix, 4 splice, 4 stack, 4 stackmmap, 4 str, 4 stream, 4 swap, 4 switch, 4 symlink, 4 sync-file, 4 syncload, 4 sysbadaddr, 4 sysinfo, 4 sysfs, 4 tee, 4 timer, 4 timerfd, 4 tlb-shootdown, 4 tmpfs, 4 tree, 4 tsearch, 4 udp, 4 udp-flood, 4 unshare, 4 uprobe, 4 urandom, 4 userfaultfd, 4 utime, 4 vdso, 4 vecmath, 4 vecwide, 4 verity, 4 vfork, 4 vforkmany, 4 vm, 4 vm-addr, 4 vm-rw, 4 vm-segv, 4 vm-splice, 4 wait, 4 wcs, 4 x86syscall, 4 xattr, 4 yield, 4 zero, 4 zlib, 4 zombie
stress-ng: info:  [4100] stress-ng-af-alg: this stressor is not implemented on this system: armv7l Linux 5.10.79 clang 10.0
stress-ng: info:  [28366] stress-ng-aiol: this stressor is not implemented on this system: armv7l Linux 5.10.79 clang 10.0
stress-ng: info:  [16148] stress-ng-binderfs: this stressor is not implemented on this system: armv7l Linux 5.10.79 clang 10.0
........................................

注：部分功能不支持此系统（this stressor is not implemented on this system）
```

